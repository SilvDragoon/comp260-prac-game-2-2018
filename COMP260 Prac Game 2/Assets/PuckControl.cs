﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PuckControl : MonoBehaviour {
    public Transform startingPos;
    private Rigidbody rigidbody;

    public AudioClip wallCollideClip;
    public AudioClip paddleCollideClip;
    private AudioSource audio;

    public LayerMask paddleLayer;

	// Use this for initialization
	void Start () {
        audio = GetComponent<AudioSource>();
        rigidbody = GetComponent<Rigidbody>();
        ResetPosition();
	}
	
    public void ResetPosition()
    {
        // teleport to the starting position
        rigidbody.MovePosition(startingPos.position);

        // stop it from moving
        rigidbody.velocity = Vector3.zero;
    }

	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        // check what we have hit
        if (paddleLayer.Contains(collision.gameObject))
        {
            // hit the paddle
            audio.PlayOneShot(paddleCollideClip);
            Debug.Log("hit paddle");
        }
        else
        {
            // hit something else
            audio.PlayOneShot(wallCollideClip);
            Debug.Log("hit wall");
        }
    }

    void OnCollisionStay(Collision collision)
    {
        //Debug.Log("Collision Stay" + collision.gameObject.name);
    }

    void OnCollisionExit(Collision collision)
    {
        //Debug.Log("Collision Exit" + collision.gameObject.name);
    }

}
