﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddle : MonoBehaviour {
    private Rigidbody rigidbody;
    public float speed = 20f;
    public float force = 10f;

    Vector3 direction = Vector3.zero;

    public Transform target;

    // Use this for initialization
    void Start () {
        rigidbody = this.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update () {
        //Debug.Log("Time = " +Time.time);
    }

    void FixedUpdate()
    {
        /*
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0, moveVertical);

        //Debug.Log("Fixed Time = " +Time.fixedTime);
        //Vector3 pos = GetMousePosition();
        //Vector3 dir = pos - rigidbody.position;
        Vector3 vel = movement.normalized * speed;

        // check is this speed is going to overshoot the target
        float move = speed * Time.fixedDeltaTime;
        float distToTarget = vel.magnitude;
        
        if (move > distToTarget)
        {
            // scale the velocity down appropriately
            vel = vel * distToTarget / move;
        }

        rigidbody.velocity = vel;

        //this.rigidbody.AddForce(dir.normalized * force);
        //rigidbody.AddForce(dir.normalized * force);
        */

        if (this.gameObject.tag == "Player")
        {
            
            direction.x = Input.GetAxis("Horizontal");
            direction.z = Input.GetAxis("Vertical");
            rigidbody.velocity = direction * speed;
        }
        else
        {
            /*
            Vector3 direction = Vector3.zero;
            direction.x = 0f;
            direction.z = Input.GetAxis("Vertical");
            rigidbody.velocity = direction * speed;
            */

            float distance = target.position.z - transform.position.z;
            
            if (distance != 0)
            {
                // going up
                direction.z = speed * distance;
            }
            rigidbody.velocity = direction * speed;
        }
    }

private Vector3 GetMousePosition()
    {
        // create a ray from the camera 
        // passing through the mouse position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        // find out where the ray intersects the XZ plane
        Plane plane = new Plane(Vector3.up, Vector3.zero);
        float distance = 0;
        plane.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }

    void OnDrawGizmos()
    {
        // draw the mouse ray
        Gizmos.color = Color.yellow;
        Vector3 pos = GetMousePosition();
        Gizmos.DrawLine(Camera.main.transform.position, pos);
    }

}
